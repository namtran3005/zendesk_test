# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* test exercises for zendesk


### How do I get set up? ###

* Summary of set up: I am using node v6.1.0

* Configuration: git clone git@bitbucket.org:namtran3005/zendesk_test.git & npm install

* Global Dependencies: babel, webpack 



Exercise 1: String compression & URI comparison

		babel-node ./scripts/__test_compress.js
		babel-node ./scripts/__test_checkurls.js


Exercise 2: TODO UI

		npm run build & babel-node app.js

		visit localhost:1122