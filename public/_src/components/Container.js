import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import update from 'react/lib/update';
import Card from './Card';
import TodoList from './TodoList';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import defaultConsts from '../consts';

class Container extends Component {
  constructor(props) {
    super(props);
    this.moveCard = this.moveCard.bind(this);
    this.state = {
      text : '',
      cards: [{
        id: 0,
        text: 'Write a cool JS library',
        status : 0,
        order : 0
      }, {
        id: 1,
        text: 'Make it generic enough',
        status : 0,
        order : 1        
      }, {
        id: 2,
        text: 'Write README',
        status : 1,
        order : 0        
      }, {
        id: 3,
        text: 'Create some examples',
        status : 1,
        order : 1        
      }, {
        id: 4,
        text: 'Spam in Twitter and IRC to promote it (note that this element is taller than the others)',
        status : 2,
        order : 0        
      }, {
        id: 5,
        text: '???',
        status : 2,
        order : 1        
      }, {
        id: 6,
        text: 'PROFIT',
        status : 2,
        order : 2        
      }]
    };
  }

  moveCard(dragOrder, hoverOrder, dragStatus, hoverStatus) { 

    const { cards } = this.state;
    let newcards = [];

    if(dragStatus === hoverStatus) {

      if( dragOrder < hoverOrder ) { // move downwards
        //we will decs order of all item from dragOrder -> hover Order
        newcards = _.map(cards, (e, i, l)=>{
          if( e.status === dragStatus && e.order > dragOrder && e.order <= hoverOrder )
          {
            return Object.assign({}, e, {
              order : e.order - 1
            });
          }
          else if ( e.status === dragStatus && e.order === dragOrder )
          {
            return Object.assign({}, e, {
              order : hoverOrder
            });            
          }
          else
          {
            return e;
          }
        });

      }
      else { // move upwards

        //we will inc order of all item from dragOrder -> hover Order
        newcards = _.map(cards, (e, i, l)=>{

          if( e.status === dragStatus && e.order < dragOrder && e.order >= hoverOrder )
          {
            return Object.assign({}, e, {
              order : e.order + 1
            });
          }
          else if ( e.status === dragStatus && e.order === dragOrder )
          {
            return Object.assign({}, e, {
              order : hoverOrder
            });            
          }
          else
          {
            return e;
          }

        });        

      }

    }
    else {
      //we simple move down every item
        newcards = _.map(cards, (e, i, l)=>{

          if( e.status === hoverStatus && e.order >= hoverOrder )
          {
            return Object.assign({}, e, {
              order : e.order + 1
            });
          }
          else if ( e.status === dragStatus && e.order === dragOrder ) // tricks here: we consider order is some kind of unid
          {
            return Object.assign({}, e, {
              order : hoverOrder,
              status : hoverStatus
            });            
          }
          else
          {
            return e;
          }

        });              

    }

    this.setState({
      cards: newcards
    });

  }  

  onChange = (e) => {
    this.setState({text: e.target.value});
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let { text, cards } = this.state;
    if(!text) return false;
    let nextItems = cards.concat([{
        id: cards.length,
        text: text,
        status : 0,
        order : cards.length
    }]);
    let nextText = '';
    this.setState({cards: nextItems, text: nextText});
  }

  render() {

    const { cards } = this.state;
    return (
      <div className="cls_lists_wrapper">
        <div className="cls_lists_header">
          <div className="cls_form_todo">
          <form className="form-inline" onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label className="cls_label_create_todo">add project</label>
              <input type="text" className="form-control" onChange={this.onChange} value={this.state.text}/>
            </div>
          </form>    
          </div>
          <div className="cls_total_todo">
            <div className="cls_list_header_info">
              <span className="cls_project_count">
                {cards.length}
              </span>
              <br/>
              <span className="cls_project_name">
                {defaultConsts["TODO_CALLED"]}
              </span>              
            </div> 
          </div>          
        </div>
        <div className="cls_lists_body row">
          <TodoList todos={cards} type={0} fnMoveCard={this.moveCard}/>
          <TodoList todos={cards} type={1} fnMoveCard={this.moveCard}/>
          <TodoList todos={cards} type={2} fnMoveCard={this.moveCard}/>        
        </div>
      </div>
    );

  }
}

export default DragDropContext(HTML5Backend)(Container);