import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import ItemTypes from './ItemTypes';
import { DragSource, DropTarget } from 'react-dnd';
import flow from 'lodash/flow';

const style = {
  border: '1px dashed gray',
  padding: '0.5rem 1rem',
  marginBottom: '.5rem',
  backgroundColor: 'white',
  cursor: 'move'
};

const cardSource = {
  beginDrag(props) {
    return {
      id: props.id,
      order: props.order,
      status : props.status
    };
  }
};

const cardTarget = {
  hover(props, monitor, component) {
    const { order : dragOrder, status : dragStatus } = monitor.getItem();
    const { order : hoverOrder, status : hoverStatus }  = props;
    const isSameList = dragStatus === hoverStatus;
     
    // Don't replace items with themselves
    if (dragOrder === hoverOrder && isSameList) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%

    if(isSameList) //we order in same list
    {
      // Dragging downwards
      if (dragOrder < hoverOrder && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragOrder > hoverOrder && hoverClientY > hoverMiddleY) {
        return;
      }      

     
      // Time to actually perform the action
      //props.moveCard(dragOrder, hoverOrder, dragStatus, hoverStatus);
      props.moveCard(dragOrder, hoverOrder, dragStatus, hoverStatus);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      monitor.getItem().order = hoverOrder;      


    }
    else
    {
      props.moveCard(dragOrder, hoverOrder, dragStatus, hoverStatus);
      monitor.getItem().order = hoverOrder; 
      monitor.getItem().status = hoverStatus;
    }



  }
};


class Card extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    isDragging: PropTypes.bool.isRequired,
    isOver: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    text: PropTypes.string.isRequired,
    moveCard: PropTypes.func.isRequired
  };

  render() {
    const { text, isDragging, connectDragSource, connectDropTarget, isOver } = this.props;
    let opacity = isDragging ? 0 : 1;

    if(isOver){
      opacity = 0.5;
    }

    return connectDragSource(connectDropTarget(
      <div className="cls_item_todo" style={{ opacity }}>
        {text}
      </div>
    ));
  }
}

export default flow(
DropTarget(ItemTypes.CARD, cardTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver()
})),
DragSource(ItemTypes.CARD, cardSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
)(Card);

