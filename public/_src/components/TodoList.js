import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import Card from './Card';
import { DropTarget } from 'react-dnd';
import ItemTypes from './ItemTypes';
import defaultConsts from '../consts';

const todoListTarget = {
  hover(props, monitor, component) {
    const { order : dragOrder, status : dragStatus } = monitor.getItem();
    const { type : hoverStatus, todos, fnMoveCard }  = props;
    let hoverOrder = 0;
    const isSameList = dragStatus === hoverStatus;

    if(isSameList){
      return; //we do nothing when in same list
    } else {
      let todosFiltered = _.filter(todos, { status : hoverStatus });
      if(!_.isEmpty(todosFiltered))
      {
        return; //let card do it jobs
      }
      else
      {
        fnMoveCard(dragOrder, hoverOrder, dragStatus, hoverStatus)
        monitor.getItem().order = hoverOrder; 
        monitor.getItem().status = hoverStatus;
      }
    }

  }
};

class TodoList extends Component {
  static propTypes = {
    type: PropTypes.number.isRequired,
    todos : PropTypes.array.isRequired,
    fnMoveCard : PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired
  };  
  constructor(props) {
    super(props);
  }
  render() {
    const strInfo = 'PROJECTS';
    const { todos, type, fnMoveCard, connectDropTarget, isOver } = this.props;
    const todosFiltered = _.filter(todos, { status : type });
    const todosFilteredSort = _.sortBy(todosFiltered, ['order']);    
    return (
      connectDropTarget(
        (<div className="col-md-4 col-sm-4 cls_toto_list">
          <div className="cls_list_header"> 
            <div className="cls_list_header_title">
              {defaultConsts['MAP_STATUS'][type]}
            </div>
            <div className="cls_list_header_info">
              <span className="cls_project_count">
                {todosFilteredSort.length}
              </span>
              <br/>
              <span className="cls_project_name">
                {defaultConsts["TODO_CALLED"]}
              </span>              
            </div>            
          </div>
          <div className="cls_list_body">
            {todosFilteredSort.map((card, i) => {
              return (
                <Card key={card.id}
                      index={card.order}
                      id={card.id}
                      text={card.text}
                      order={card.order}
                      status={card.status}
                      moveCard={fnMoveCard} />
              );
            })}          
          </div>          
        </div>)
      )
    )
  }
}

export default DropTarget(ItemTypes.CARD, todoListTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver()
}))(TodoList);