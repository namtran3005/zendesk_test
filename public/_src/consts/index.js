export default {
  MAP_STATUS : {
  	0 : "To do",
  	1 : "In Progress",
  	2 : "Done" 	  	
  },
  TODO_CALLED : 'PROJECTS'
};