import test from 'tape';
import checkURIs from './checkurls';

test('empty and empty should be equal', (assert) => {
  const url1 = "";
  const url2 = '';
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    'empty url should be equal');
  assert.end();
});

test('Comparisons of host names must case-insensitive', (assert) => {
  const url1 = "http://abc.com:80/~smith/home.html";
  const url2 = "http://ABC.com/%7Esmith/home.html";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});

test('Comparisons of scheme names must be case-insensitive', (assert) => {
  const url1 = "HTTP://abc.COM:80/~smith/home.html";
  const url2 = "http://ABC.com/%7Esmith/home.html";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});

test('Paths may contain traversal tokens', (assert) => {
  const url1 = "http://abc.com/drill/down/foo.html";
  const url2 = "http://abc.com/drill/further/../down/./foo.html";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});

test('Query string parameters must be equivalent in arbitrary order', (assert) => {
  const url1 = "http://abc.com/foo.html?a=1&b=2";
  const url2 = "http://abc.com/foo.html?b=2&a=1";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});

test('BUT query string arguments of the same name must be listed in the same order in both URIs to be equivalent', (assert) => {
  const url1 = "http://abc.com/foo.html?a=1&b=2&a=3";
  const url2 = "http://abc.com/foo.html?a=3&a=1&b=2";
  const expected = false;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should not be equal ${url2}`);
  assert.end();
});

test('Same order should be ok', (assert) => {
  const url1 = "http://abc.com/foo.html?a=1&b=2&a=3";
  const url2 = "http://abc.com/foo.html?a=1&a=3&b=2";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});

test('It should know auth', (assert) => {
  const url1 = "http://nam.tran:pass@abc.com/drill/down/foo.html";
  const url2 = "HTTP://nam.tran:pass@ABC.COM/drill/further/.././down/./foo.html";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});

test('Auth must be case-sensitive', (assert) => {
  const url1 = "http://nam.tran:pass@abc.com/drill/down/foo.html";
  const url2 = "HTTP://nam.tran:PASS@ABC.COM/drill/further/.././down/./foo.html";
  const expected = false;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should not be equal ${url2}`);
  assert.end();
});


test("can't move out site root dir", (assert) => {
  const url1 = "http://nam.tran:pass@abc.com/drill/down/../../.././../../test/../../foo.html";
  const url2 = "HTTP://nam.tran:pass@ABC.COM/foo.html";
  const expected = true;
  const actual = checkURIs(url1, url2);
  assert.equal(actual, expected,
    `${url1} should be equal ${url2}`);
  assert.end();
});






