'use strict';
const not = (fn) => (x) => !fn(x)
const isEmpty = (x) => x ? false : true

function parseUrl(url) {
	let regScheme = "^((([^:/?#]+)://)?)"; 				//the schema parts in group 1, 2, 3
	let regAuthor = "((([^:/?#@]+):([^:/?#@]+)@)?)"; 	//the author parts in group 4,5,6,7
	let regDomain = "([^/:?#]*)?"; 						//the domain parts in group 8 
	let regPort = "(:([0-9]*))?"; 						//the domain parts in group 9 10	
	let regPath = "([^?#]*)"; 							//the path parts in group 11 
	let regQuery = "(\\?([^#]*))?"; 					//the query parts in group 13
	let regHash = "(#(.*))?"; 							//the hash parts in group 15
    let pattern = RegExp(regScheme + regAuthor + regDomain + regPort + regPath + regQuery + regHash);
    var matches =  url.match(pattern);
    return {
        scheme: matches[3] || '',
        authority: {
        	user : matches[6],
        	pass : matches[7]
        },
        domain: matches[8] || '',
        port : matches[10] || '80', 
        path: matches[11] || '',
        query: matches[13] || '',        
        fragment: matches[15] || ''
    };
}

function normalizePath(path){
	
	let arrPath = path.split('/');
	let i = 0;
	arrPath = arrPath.filter(not(isEmpty)); /* normalizePath slash so /have/slash/ will be come have/slash */ 

	for(; i < arrPath.length; i++ )
	{
		let e = arrPath[i];
		if( e === '..' )
		{
			if( i === 0 )
			{
				/* We can't let they access parents of root dir 
				** If they try, they simple be returned into root dir
				** example ( /../../ <===> / )				 
				*/
				arrPath.splice(i, 1);
				i = i - 1								
			}
			else
			{
				arrPath.splice(i-1, 2); //remove itself and element before it
				i = i - 2 
			}
		}
		else if (e === '.')
		{
			arrPath.splice(i, 1); //remove itself only
			i = i - 1 			
		}
	}
	return arrPath.join('/');
}

function parseQueryStr(query){
	let arrVar = query.split('&');
	let mapKey = {};
	for(let i = 0; i < arrVar.length; i++ )
	{
		let e = arrVar[i];
		let [key, val] = e.split('=');
		if(key && val)
		{
			if(mapKey[key])
			{
				mapKey[key].push(val);
			} 
			else
			{
				mapKey[key] = [];
				mapKey[key].push(val);					
			}
		}
	}	
	return mapKey;
}

function arrayEqual (array1, array2) {
	if(!array1 || !array2) return false;
  if (array1.length !== array2.length)
    return false;
  for (let i = 0; i < array1.length ; i++) {
	  if(decodeURI(array1[i]) !== decodeURI(array2[i])) return false;     
  }       
  return true;
}

function queryStrEqual(query1, query2){
	let objQuery1 = parseQueryStr(query1);
	let objQuery2 = parseQueryStr(query2);
	for (let prop in objQuery1) {
		if( !arrayEqual(objQuery1[prop], objQuery2[prop]) )
			return false;
	}
	return true;
}

export default function checkURIs(url1, url2){
	let objUrl1 = parseUrl(url1);
	let objUrl2 = parseUrl(url2);

	let normalizePath1 = decodeURI(normalizePath(objUrl1['path']));
	let normalizePath2 = decodeURI(normalizePath(objUrl2['path']));

	let { query : query1 } = objUrl1;
	let { query : query2 } = objUrl2;	 

	return ( 
		objUrl1['scheme'].toLowerCase() === objUrl2['scheme'].toLowerCase() &&
		(objUrl1['authority']['user'] === objUrl2['authority']['user'] && objUrl1['authority']['pass'] === objUrl2['authority']['pass']) &&
		objUrl1['domain'].toLowerCase() === objUrl2['domain'].toLowerCase() &&
		objUrl1['port'] === objUrl2['port'] &&
		normalizePath1 === normalizePath2 &&
		queryStrEqual(query1, query2)
	) ? true : false;	
}