import test from 'tape';
import compress from './compress';

test('empty input string', (assert) => {
  const expected = '';
  const actual = compress();
  assert.equal(actual, expected,
    'Given empty string, return value should be empty');

  assert.end();
});

test('test aaaabbaaaababbbcccccccccccc', (assert) => {
  const input = "aaaabbaaaababbbcccccccccccc";
  const expected = 'a4b2a4b1a1b3c12';
  const actual = compress(input);
  assert.equal(actual, expected, `Given ${input}, return value should be ${expected}`);
  assert.end();
});

test('test ckdaaabbbccckdbkkk', (assert) => {
  const input = "ckdaaabbbccckdbkkk";
  const expected = 'c1k1d1a3b3c3k1d1b1k3';
  const actual = compress(input);
  assert.equal(actual, expected, `Given ${input}, return value should be ${expected}`);
  assert.end();
});