'use strict';

export default function compress(str='') {
	if (!str) return '';
	let strResult = '';
	let currentIndex = 0;
	let currentChar = str[currentIndex];
	let nextIndex = currentIndex + 1;

	while(true){
		if( str[nextIndex] !== currentChar ) {
			let diff = nextIndex - currentIndex;
			strResult += currentChar + diff;
			if(nextIndex >= str.length) break;
			currentIndex = nextIndex;
			currentChar = str[nextIndex];			
		}
		nextIndex++;
	}
	return strResult;
}

