var express = require('express');
var router = express.Router();

module.exports = function(app, strBase){
	/* GET users listing. */
	router.get('/', function(req, res, next) {
		res.render('index', { title: 'Hashtag.vn' });
	});
    app.use(strBase, router);
}
