var path = require('path');
var webpack = require('webpack');

module.exports = {
  quiet: true,
  noInfo: true,
  entry: {
    vendor: ["react", "react-dom"],  
    app: path.resolve(__dirname, 'public/_src', 'app.js'),
  },
  output: {
    path: path.join(__dirname, 'public/_bin'),
    filename: 'main.js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.bundle.js' })
  ],
  webpackMiddleware: {
    noInfo: true
  },
  module: {
    loaders: [{
      cacheDirectory: true,
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    }]
  }
};