var express = require('express');
var Promise = require('bluebird');
var cors = require('cors')
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var debug = require('debug')('hashtest:server');
var initRoutes = require("./services/init_routes.js");
var corsOptions = {
  origin: '*'
};
var app = express();
const APP_PORT = 1122;
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/public', express.static(path.join(__dirname, 'public')));

function async(makeGenerator){
    return function (){
      var generator = makeGenerator.apply(this, arguments)
      function handle(result){ // { done: [Boolean], value: [Object] }
        if (result.done) return result.value
        return result.value.then(function (res){
          return handle(generator.next(res))
        }, function (err){
          return handle(generator.throw(err))
        })
      }
      return handle(generator.next())
    }
}


((async(function* (){

	yield Promise.resolve(true);
	yield initRoutes(app);

  app.set('port', process.env.PORT || APP_PORT);
  var server = http.createServer(app);
  var server = server.listen(app.get('port'), function(err) {
    if(err)
    {
    	 throw err;
    }
    console.log('Express server listening on port ' + server.address().port);
  });        

}))()).catch(function(e){
		console.log(e);
		process.exit(1);
});